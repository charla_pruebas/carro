package com.pruebas.carro.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.entrada.IPageableDto;
import com.pruebas.carro.dto.salida.OCarroDto;
import com.pruebas.carro.entity.CarroEntity;
import com.pruebas.carro.repositorio.CarroRepositorio;
import com.pruebas.carro.service.impl.CarroServiceImpl;
import com.pruebas.carro.error.Encontrado;
import com.pruebas.carro.error.NoEncontrado;
import com.pruebas.carro.mapper.CarroMapper;

//pruebas unitarias
//todas las pruebas son simultaneas
@ExtendWith(MockitoExtension.class)
class CarroServiceImplTest {

	@InjectMocks
	CarroServiceImpl carroServiceImpl;
	
	@Mock
	protected CarroMapper carroMapper;
	
	@Mock
	CarroRepositorio carroRepositorio;
	
	@Mock
	CarroEntity carroEntity;
	
	@Mock
	OCarroDto oCarroDto;
	
	@Mock
	ICarroDto iCarroDto;
	
	@Mock
	IPageableDto iPageableDto;
	
	private static final int ID = 1;	
	
	@Test
	void findAllTest() {				
		List<CarroEntity> carrosEntity = new ArrayList<>();
		carrosEntity.add(carroEntity);
		Mockito.when(carroRepositorio.findAll()).thenReturn(carrosEntity);

		Assertions.assertEquals(carrosEntity.size(), carroServiceImpl.findAll().size());
	}
	
	@Test
	void getAllPagesTest() {
		//Arrange
		Integer pageNo = 0;
		Integer pageSize = 1;
		String orderBy = "id";
		Integer cantidadDeRegistros = 1;		
		List<CarroEntity> carrosEntity = new ArrayList<>();
		carrosEntity.add(carroEntity);		
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(orderBy));		
		Page<CarroEntity> carrosPageEntity = new PageImpl<>(carrosEntity, PageRequest.of(pageNo, pageSize), cantidadDeRegistros);
		
        //Act
		Mockito.when(iPageableDto.getPageNo()).thenReturn(pageNo);
		Mockito.when(iPageableDto.getPageSize()).thenReturn(pageSize);
		Mockito.when(iPageableDto.getSortBy()).thenReturn(orderBy);
		Mockito.when(carroRepositorio.findAll(paging)).thenReturn(carrosPageEntity);
		
		//Assert
		Assertions.assertEquals(carrosEntity.size(), carroServiceImpl.getAllPages(iPageableDto).getSize());
	}
	
	@Test
	void findOneTestWhenCarroDtoIsNotPresent() {	
		//Arrange	
		
        //Act
		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		//Assert
		Assertions.assertThrows(NoEncontrado.class, () -> carroServiceImpl.findOne(ID));
	}
	
	@Test
	void findOneTestWhenCarroDtoIsPresent() {	
		//Arrange	
		
        //Act
		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.of(carroEntity));
		Mockito.when(carroMapper.convertCarroEntityToOCarroDto(carroEntity)).thenReturn(oCarroDto);
		Mockito.when(oCarroDto.getId()).thenReturn(ID);
		
		//Assert
		Assertions.assertEquals(ID, carroServiceImpl.findOne(ID).getId());
	}

	@Test
	void saveTestWhenIdIsPresent() {
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(ID);
		
		//Assert		
		Assertions.assertThrows(Encontrado.class, () -> carroServiceImpl.save(iCarroDto));
	}
	
	@Test
	void saveTestWhenCarroDtoIsNotPresent() {
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(0);
		Mockito.when(oCarroDto.getId()).thenReturn(0);
		Mockito.when(carroMapper.convertICarroDtoToCarroEntity(iCarroDto)).thenReturn(carroEntity);
		Mockito.when(carroRepositorio.findByMarcaAndModelo(iCarroDto.getMarca(), iCarroDto.getModelo())).thenReturn(Optional.ofNullable(null));
		Mockito.when(carroRepositorio.save(carroEntity)).thenReturn(carroEntity);
		Mockito.when(carroMapper.convertCarroEntityToOCarroDto(carroEntity)).thenReturn(oCarroDto);
		
		//Assert		
		Assertions.assertEquals(oCarroDto.getId(), carroServiceImpl.save(iCarroDto).getId());
	}
	
	@Test
	void saveTestWhenCarroDtoIsPresent() {		
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(ID);
		
		//Assert		
		Assertions.assertThrows(Encontrado.class, () -> carroServiceImpl.save(iCarroDto));
	}
	
	@Test
	void updateTestWhenCarroDtoIsNotPresent() {
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(ID);

		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		//Assert		
		Assertions.assertThrows(NoEncontrado.class, () -> carroServiceImpl.update(iCarroDto));
	}
	
	@Test
	void updateTestWhenCarroDtoIsPresent() {
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(ID);
		Mockito.when(oCarroDto.getId()).thenReturn(ID);

		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.of(carroEntity));

		Mockito.when(carroRepositorio.findByIdNotAndMarcaAndModelo(ID, iCarroDto.getMarca(), iCarroDto.getModelo())).thenReturn(Optional.ofNullable(null));
		
		Mockito.when(carroMapper.convertICarroDtoToCarroEntity(iCarroDto)).thenReturn(carroEntity);
		Mockito.when(carroMapper.convertCarroEntityToOCarroDto(carroEntity)).thenReturn(oCarroDto);
		Mockito.when(carroRepositorio.save(carroEntity)).thenReturn(carroEntity);
		
		//Assert		
		Assertions.assertEquals(ID, carroServiceImpl.update(iCarroDto).getId());
	}
	
	@Test
	void updateTestWhenCodigoIsPresent() {
		//Arrange	
		
        //Act
		Mockito.when(iCarroDto.getId()).thenReturn(ID);

		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.of(carroEntity));

		Mockito.when(carroRepositorio.findByIdNotAndMarcaAndModelo(ID, iCarroDto.getMarca(), iCarroDto.getModelo())).thenReturn(Optional.ofNullable(carroEntity));
		
		//Assert		
		Assertions.assertThrows(Encontrado.class, () -> carroServiceImpl.update(iCarroDto));
	}
	
	@Test
	void deleteTestWhenCarroDtoIsNotPresent() {
		//Arrange	
		
        //Act
		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		//Assert		
		Assertions.assertThrows(NoEncontrado.class, () -> carroServiceImpl.deleteOne(ID));
	}
	
	@Test
	void deleteTestWhenCarroDtoIsPresent() {
		//Arrange	
		
        //Act
		Mockito.when(carroRepositorio.findById(ID)).thenReturn(Optional.of(carroEntity));
		Mockito.when(carroMapper.convertCarroEntityToOCarroDto(carroEntity)).thenReturn(oCarroDto);
		Mockito.when(oCarroDto.getId()).thenReturn(ID);
		
		//Assert		
		Assertions.assertEquals(ID, carroServiceImpl.deleteOne(ID).getId());
	}
	
	@Test
	void findAllByIdsTest() {
		//Arrange	
		List<Integer> listId = new ArrayList<>();
		listId.add(ID);
		
		List<CarroEntity> nCarros = new ArrayList<>();
		nCarros.add(carroEntity);
		
        //Act
		Mockito.when(carroMapper.convertCarroEntityToOCarroDto(carroEntity)).thenReturn(oCarroDto);
		
		Mockito.when(carroRepositorio.findAllById(listId)).thenReturn(nCarros);
		
		//Assert
		Assertions.assertEquals(nCarros.size(), carroServiceImpl.findAllByIds(listId).size());
	}

}
