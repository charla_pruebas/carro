package com.pruebas.carro;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.pruebas.carro.controller.CarroController;

@SpringBootTest
class CarroApplicationTests {
	
	@Autowired
	 private CarroController carroController;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(carroController);
	}

}
