package com.pruebas.carro.controller;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.pruebas.carro.entity.CarroEntity;
import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.salida.OCarroDto;
import com.pruebas.carro.repositorio.CarroRepositorio;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pruebas.carro.error.Encontrado;
import com.pruebas.carro.error.NoEncontrado;
import com.google.gson.Gson;

//pruebas de integración
//todas las pruebas son simultaneas
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
class CarroControllerTest {
	private static final String PREFIJO_URL = "/carros";
	private static final String MARCA = "mymarca";
	private static final String MARCA2 = "mymarca2";
	private static final Integer MODELO = 2021;
	
	private static final String TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICItZmg0ZnZXOG9iaUdjQWdqdGo0VjJqTGpxR1JKY0pXS1hSTVBYZ1FxMy1RI"
			+ "n0.eyJleHAiOjE2Mjg4MTk2MjksImlhdCI6MTYyODc4MzYyOSwianRpIjoiN2MwZDYwYjEtZTM1Zi00MTRkLWE2NmYtN2U2MDJlNWViYWQ2IiwiaXNzIjoiaHR0cDovL2xvY2Fs"
			+ "aG9zdDo4MDgwL2F1dGgvcmVhbG1zL2dlbmVzaXMiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiMTViYTZlY2ItZjdmNC00YmVmLWIwMmMtNzVkZGUzZDIyZjdjIiwidHlwIjoiQmV"
			+ "hcmVyIiwiYXpwIjoiZ2VuZXNpc2JrIiwic2Vzc2lvbl9zdGF0ZSI6ImZmMzk5NGNmLTI3NTQtNDkxMS1iMzdhLWIwODQ1YWU0NTk2OSIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3"
			+ "MiOnsicm9sZXMiOlsiZGVmYXVsdC1yb2xlcy1nZW5lc2lzIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImdlbmVza"
			+ "XNiayI6eyJyb2xlcyI6WyJhZG1pbiIsInVzdWFyaW8iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXct"
			+ "cHJvZmlsZSJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoibHVpcyJ9.QOx6nrKidwA9I8AWdg"
			+ "h3nupmcVHqjs5mxdzOjeAhXIwwvODmTB0lztJ5kbK_VJ0ev3U1vEHNWXJP1xrB9uyhq2kmEytTCNCzbhDg5mz_eiYPoIOTzNre2bJuSqt40U5is9LgV-Ixz4zUvKIIuCSVTkbmx"
			+ "0Uysv0rWkabefZHrTgYmRvgz4KXiFWOKH8uvzK-Z-R2OIOJa11orqWgYe3nL4ckHm9VTGy9q5xNNoI9-1UA0ChFa2oCKrlbCcUGEwRbmG_vGlcDk8SH7bNjac8lz5gBmYRGg8Cw"
			+ "bMQAZ_iH3BpeCJ9NzrocWEd2-PuWmnJRPQMklelZA7jV_Cg_UJH8xg";
	
	private static Integer id;
	private static Integer idDelete;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	CarroRepositorio carroRepositorio;

	@Test
	@Order(7)
	void listTest() throws Exception {
		//Arrange
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/all")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(uri.toUriString())
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
        	.andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(8)
	void getAllPagesTest() throws Exception {
		//Arrange
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL)
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(uri.toUriString()).param("pageNo", "0").param("pageSize", "1").param("sortBy", "id")
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
        	.andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(5)
	void getTestWhenCarroDtoIsPresent() throws Exception {
		//Arrange
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/{id}")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(uri.toUriString(), id)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)))
            .andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(6)
	void getTestWhenCarroDtoIsNotPresent() throws Exception {
		//Arrange
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/{id}")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(uri.toUriString(), idDelete)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado))
            .andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(2)
	void addTestWhenIdIsPresent() throws Exception {
		//Arrange
		ICarroDto iCarroDto = ICarroDto.builder()
				.id(id)
				.marca(MARCA)
				.modelo(MODELO)
				.build();

        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL)
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(uri.toUriString())
				.content(objectMapper.writeValueAsString(iCarroDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof Encontrado))
            .andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(1)
	void addTestWhenIdIsNotPresent() throws Exception {
		//Arrange
		ICarroDto iCarroDto = ICarroDto.builder()
				.id(0)
				.marca(MARCA)
				.modelo(MODELO)
				.build();

        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL)
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(uri.toUriString())
				.content(objectMapper.writeValueAsString(iCarroDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isCreated())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.notNullValue()))
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.not("")))
			.andExpect(MockMvcResultMatchers.jsonPath("$.marca", CoreMatchers.is(MARCA)))
            .andDo(MockMvcResultHandlers.print());
		//Adicional
		MvcResult mvcResult = result.andReturn();
		String contentAsString = mvcResult.getResponse().getContentAsString();
		OCarroDto CarroDtoResponse = objectMapper.readValue(contentAsString, OCarroDto.class);
		
		id = CarroDtoResponse.getId();
		CarroEntity carroEntity = carroRepositorio.save(CarroEntity.builder()
				.id(0)
				.marca(MARCA2)
				.modelo(MODELO)
				.build());
		idDelete = carroEntity.getId();
		carroRepositorio.delete(carroEntity);
	}

	@Test
	@Order(3)
	void updateTestWhenCarroDtoIsPresent() throws Exception {
		//Arrange
		ICarroDto iCarroDto = ICarroDto.builder()
				.id(id)
				.marca(MARCA)
				.modelo(MODELO)
				.build();

        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL)
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.put(uri.toUriString())
				.content(objectMapper.writeValueAsString(iCarroDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)))
            .andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(4)
	void updateTestWhenCarroDtoIsNotPresent() throws Exception {
		//Arrange
		ICarroDto iCarroDto = ICarroDto.builder()
				.id(idDelete)
				.marca(MARCA)
				.modelo(MODELO)
				.build();

        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL)
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.put(uri.toUriString())
				.content(objectMapper.writeValueAsString(iCarroDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado))
            .andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@Order(10)
	void deleteTestWhenCarroDtoIsPresent() throws Exception {
		//Arrange
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/{id}")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.delete(uri.toUriString(), id)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)))
            .andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	@Order(11)
	void deleteTestWhenCarroDtoIsNotPresent() throws Exception {
		//Arrange	
        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/{id}")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.delete(uri.toUriString(), idDelete)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado))
            .andDo(MockMvcResultHandlers.print());
	}

	@Test
	@Order(9)
	void listByIdTest() throws Exception {
		//Arrange		
		List<Integer> ids = new ArrayList<>();
		ids.add(id);

        UriComponents uri = UriComponentsBuilder
                .fromPath(PREFIJO_URL + "/ids")
                .build();
        //Act
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(uri.toUriString())
				.content(new Gson().toJson(ids))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		//Assert
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(id)))
            .andDo(MockMvcResultHandlers.print());
	}

}
