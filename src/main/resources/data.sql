DROP TABLE IF EXISTS carro;
CREATE TABLE carro (
  id int NOT NULL AUTO_INCREMENT,
  marca varchar(255) NOT NULL,
  modelo int NOT NULL,
  actived bit(1) DEFAULT NULL,
  PRIMARY KEY (id)
);


INSERT INTO carro (id, marca, modelo, actived)
VALUES
('1','chevrolet',2011,true),
('2','mazda',2021,true),
('3','mazda',2011,true);