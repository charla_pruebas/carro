package com.pruebas.carro.error;

public class Encontrado extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public Encontrado(String mensaje) {
		super(mensaje);
	}

}
