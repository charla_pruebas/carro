package com.pruebas.carro.error;

public class NoEncontrado extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoEncontrado(String mensaje) {
		super(mensaje);
	}

}
