package com.pruebas.carro.error;

public class FaltaArgumento extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FaltaArgumento(String mensaje) {
		super(mensaje);
	}

}
