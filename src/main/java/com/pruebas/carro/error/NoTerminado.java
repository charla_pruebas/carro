package com.pruebas.carro.error;

public class NoTerminado extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoTerminado(String mensaje) {
		super(mensaje);
	}

}
