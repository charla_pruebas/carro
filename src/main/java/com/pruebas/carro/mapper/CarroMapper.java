package com.pruebas.carro.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.salida.OCarroDto;
import com.pruebas.carro.entity.CarroEntity;

@Mapper
public interface CarroMapper {
	
	CarroMapper INSTANCE = Mappers.getMapper(CarroMapper.class);

	@Mapping(target = "actived", ignore = true)
	CarroEntity convertICarroDtoToCarroEntity(ICarroDto iCarroDto);

	@Mapping(target = "actived", ignore = true)
	CarroEntity convertOCarroDtoToCarroEntity(OCarroDto oCarroDto);
	
	ICarroDto convertCarroEntityToICarroDto(CarroEntity carroEntity);
	
	OCarroDto convertCarroEntityToOCarroDto(CarroEntity carroEntity);

}
