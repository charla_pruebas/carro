package com.pruebas.carro.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.entrada.IPageableDto;
import com.pruebas.carro.dto.salida.OCarroDto;
import com.pruebas.carro.service.CarroService;
import com.pruebas.carro.utilidades.Convertir;
import com.pruebas.carro.error.FaltaArgumento;

@RestController
@RequestMapping("/carros")
@RolesAllowed("admin")
public class CarroController {

    @Autowired
    CarroService carroService;
    
    @GetMapping("/all")
    public ResponseEntity<List<OCarroDto>> list() {
    	return new ResponseEntity<>(carroService.findAll(), HttpStatus.OK); 
    }
    
    @GetMapping
    public ResponseEntity<Page<OCarroDto>> getAllPages(IPageableDto iPageableDto) {
    	return new ResponseEntity<>(carroService.getAllPages(iPageableDto), HttpStatus.OK); 
    }

    @GetMapping("/{id}")
    public ResponseEntity<OCarroDto> get(@PathVariable("id") Integer id) {
    	return new ResponseEntity<>(carroService.findOne(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OCarroDto> add(@Valid @RequestBody ICarroDto iCarro, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convertir.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(carroService.save(iCarro), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<OCarroDto> update(@Valid @RequestBody ICarroDto iCarro, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convertir.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(carroService.update(iCarro), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OCarroDto> delete(@PathVariable("id") Integer id) {  
    	return new ResponseEntity<>(carroService.deleteOne(id),HttpStatus.OK);
    }

    @PostMapping("/ids")
    public ResponseEntity<List<OCarroDto>> listById(@RequestBody List<Integer> listId) {
    	return new ResponseEntity<>(carroService.findAllByIds(listId), HttpStatus.OK);
    }

}
