package com.pruebas.carro.dto.salida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OCarroDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	

	private Integer id;
	private String marca;
	private Integer modelo;

}
