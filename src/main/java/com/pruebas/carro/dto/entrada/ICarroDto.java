package com.pruebas.carro.dto.entrada;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.Range;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ICarroDto implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@NotEmpty(message = "Marca no puede ser vacio o nulo")
	private String marca;
	@Range(min = 1500, max = 2500, message = "Model debe estar entre 1500 y 2500")
	private Integer modelo;	

}
