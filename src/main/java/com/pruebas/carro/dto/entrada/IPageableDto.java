package com.pruebas.carro.dto.entrada;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IPageableDto {

	@Builder.Default
	private Integer pageNo = 0;
	@Builder.Default
	private Integer pageSize = 10;
	@Builder.Default
	private String sortBy = "id";

}
