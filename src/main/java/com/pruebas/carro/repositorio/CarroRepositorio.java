package com.pruebas.carro.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pruebas.carro.entity.CarroEntity;

@Repository
public interface CarroRepositorio extends JpaRepository<CarroEntity, Integer> {
	Optional<CarroEntity> findByMarcaAndModelo(String marca, Integer modelo);
	Optional<CarroEntity> findByIdNotAndMarcaAndModelo(Integer id, String marca, Integer modelo);

}
