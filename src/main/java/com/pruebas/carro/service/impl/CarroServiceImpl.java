package com.pruebas.carro.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.entrada.IPageableDto;
import com.pruebas.carro.dto.salida.OCarroDto;
import com.pruebas.carro.entity.CarroEntity;
import com.pruebas.carro.repositorio.CarroRepositorio;
import com.pruebas.carro.service.CarroService;
import com.pruebas.carro.error.Encontrado;
import com.pruebas.carro.error.NoEncontrado;
import com.pruebas.carro.mapper.CarroMapper;
import com.pruebas.carro.utilidades.FormatString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(value = "carroService")
public class CarroServiceImpl implements CarroService {
	
	private static final String RECURSO = "carro";
	private static final String IDENTIFICADOR = "código";
	
	@Autowired
	protected CarroMapper carroMapper;
	
	@Autowired
	private CarroRepositorio carroRepositorio;

	@Override
	public List<OCarroDto> findAll() {
		List<OCarroDto> nCarrosDto = new ArrayList<>();
		List<CarroEntity> nCarros = carroRepositorio.findAll();
		for(CarroEntity carro : nCarros) {
			nCarrosDto.add(carroMapper.convertCarroEntityToOCarroDto(carro));
		}
		log.info("se consulta listado de carros");
        return nCarrosDto;
	}

	@Override
	public Page<OCarroDto> getAllPages(IPageableDto iPageableDto){
		List<OCarroDto> nCarrosDto = new ArrayList<>();
        Pageable paging = PageRequest.of(iPageableDto.getPageNo(), iPageableDto.getPageSize(), Sort.by(iPageableDto.getSortBy())); 
        Page<CarroEntity> nCarros = carroRepositorio.findAll(paging);
		for(CarroEntity carro : nCarros) {
			nCarrosDto.add(carroMapper.convertCarroEntityToOCarroDto(carro));
		}
		log.info("se consulta paginado de carros");
		return new PageImpl<>(nCarrosDto, nCarros.getPageable(), nCarros.getTotalElements());
	}

	@Override
	public OCarroDto findOne(Integer id) {
		Optional<CarroEntity> nCarroOpt = carroRepositorio.findById(id);
		if (nCarroOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		log.info("se consulta carro {}", id);
        return carroMapper.convertCarroEntityToOCarroDto(nCarroOpt.get());
	}

	@Override
	public OCarroDto save(ICarroDto iCarro) {	
		if (iCarro.getId() != 0) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}
		if (carroRepositorio.findByMarcaAndModelo(iCarro.getMarca(), iCarro.getModelo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCarro.getMarca() + " " + iCarro.getModelo()));
		}
		CarroEntity nCarro = carroMapper.convertICarroDtoToCarroEntity(iCarro);
		nCarro = carroRepositorio.save(nCarro);
		log.info("se crea carro con el id: {}",  nCarro.getId());
        return carroMapper.convertCarroEntityToOCarroDto(nCarro);
	}

	@Override
	public OCarroDto update(ICarroDto iCarro) {	
		if (carroRepositorio.findById(iCarro.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", Integer.toString(iCarro.getId())));
		}
		if (carroRepositorio.findByIdNotAndMarcaAndModelo(iCarro.getId(), iCarro.getMarca(), iCarro.getModelo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCarro.getMarca() + " " + iCarro.getModelo()));
		}
		CarroEntity nCarro = carroMapper.convertICarroDtoToCarroEntity(iCarro);
		nCarro = carroRepositorio.save(nCarro);
		log.info("se actualiza carro con el id: {}", nCarro.getId());
        return carroMapper.convertCarroEntityToOCarroDto(nCarro);
	}

	@Override
	public OCarroDto deleteOne(Integer id) {
		Optional<CarroEntity> nCarroOpt = carroRepositorio.findById(id);
		if (nCarroOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		carroRepositorio.deleteById(id);
		log.info("se elimina carro con el id: {}", id);
		return carroMapper.convertCarroEntityToOCarroDto(nCarroOpt.get());
	}

	@Override
	public List<OCarroDto> findAllByIds(List<Integer> listId) {
		List<OCarroDto> nCarrosDto = new ArrayList<>();
		Iterable<CarroEntity> nCarros = carroRepositorio.findAllById(listId);
		for(CarroEntity carro : nCarros) {
			nCarrosDto.add(carroMapper.convertCarroEntityToOCarroDto(carro));
		}
		log.info("se consulta listado de carros según Ids");
        return nCarrosDto;
	}

}
