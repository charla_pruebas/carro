package com.pruebas.carro.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.pruebas.carro.dto.entrada.ICarroDto;
import com.pruebas.carro.dto.entrada.IPageableDto;
import com.pruebas.carro.dto.salida.OCarroDto;

public interface CarroService {
	public List<OCarroDto> findAll();
	public Page<OCarroDto> getAllPages(IPageableDto iPageableDto);
	public OCarroDto findOne(Integer id);
	public OCarroDto save(ICarroDto iCarro);
	public OCarroDto update(ICarroDto iCarro);
	public OCarroDto deleteOne(Integer id);
	public List<OCarroDto> findAllByIds(List<Integer> listId);
}
