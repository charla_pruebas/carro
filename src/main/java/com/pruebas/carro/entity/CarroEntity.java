package com.pruebas.carro.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "carro", indexes = {@Index(columnList = "marca, modelo, actived", unique = true)})
@SQLDelete(sql = "UPDATE carro SET actived = NULL WHERE id=?")
@Where(clause = "actived=true")
public class CarroEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable=false)
	private String marca;
	
	@Column(nullable=false)
	private Integer modelo;
	
	@Column
	@Builder.Default
	private boolean actived = Boolean.TRUE;

}
