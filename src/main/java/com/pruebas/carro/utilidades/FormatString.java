package com.pruebas.carro.utilidades;

public class FormatString {
	
	private FormatString() {
		throw new IllegalStateException();
	}
	
	public static String noExisteString(String recurso, String identificador, String valorIdentificador) {
		return String.format("No existe %s con el %s: %s", recurso, identificador, valorIdentificador);
	}
	
	public static String yaExisteString(String recurso, String identificador, String valorIdentificador) {
		return String.format("Ya existe %s con el %s: %s", recurso, identificador, valorIdentificador);
	}
	
	public static String seEnvioString(String identificador) {
		return String.format("Se envió %s", identificador);
	}
	
	public static String noSeEnvioString(String identificador) {
		return String.format("No se envió %s", identificador);
	}
	
	public static String noEsTipoString(String identificador, String tipo, String valorIdentificador) {
		return String.format("%s no es tipo %s: %s", identificador, tipo, valorIdentificador);
	}
	
	public static String noOperacionString(String operacion, String recurso) {
		return String.format("No se %s %s", operacion, recurso);
	}

}
